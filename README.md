# Codes for Arduino - Tukang Obat

## Sensors

Listed below are sensors used in this project and its uses.

- TCRT5000 (IR sensor): Detect the presence of cup under the dispenser.
- KY-008 and it's receiver: Check if the medicine has successfully dispensed.
- Thermometer and hygrometer: Check if the temperature or humidity of the containment unit has passed the optimal temperature and humidity threshold.
- PING))) (Ultrasonic proximity sensor): Check the presence of customer.
- Keypad: Receives recipe codes from customer.
- Trimpot: Calibrate and align lock and transport layer.

## Actuators

Listed below are actuators used in this project and its uses.

- 2 x servo motor: Move lock and transport layer of the dispenser.
- Fan: Turns on if the temperature or the humidity of the containment unit has passed the optimal temperature and humidity threshold.
- LCD: Shows the information to customer.