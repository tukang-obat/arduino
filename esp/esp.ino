// reference: https://www.emqx.com/en/blog/esp32-connects-to-the-free-public-mqtt-broker
#include <WiFi.h>
#include <PubSubClient.h>

// WiFi
const char *ssid = ""; // TODO
const char *password = "";  // TODO

// MQTT Broker
const char *mqtt_broker = ""; // TODO
const char *topicFromMachine1 = "fromMachine/token";
const char *topicToMachine1 = "toMachine/resep";
const char *topicFromMachine2 = "fromMachine/success";
const char *mqtt_username = ""; // TODO
const char *mqtt_password = ""; // TODO
const int mqtt_port = 1883;

String token = "";

WiFiClient espClient;
PubSubClient client(espClient);

bool published1 = 0;
bool received2 = 1;
unsigned long lastPublished1;

void initWifi() {
  // connecting to a WiFi network
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}

void resetPublished1() {
  lastPublished1 = 0;
  published1 = 0;
  token = "";
}

void reconnect() {      // This is for the MQTT connection
  // Loop until we're reconnected
  while (!client.connected()) {
    if (client.connect("arduinoClient", mqtt_username, mqtt_password)) {
      client.subscribe(topicToMachine1); 
    } else {
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char *topic, byte *payload, unsigned int length) {
    String resep = "";
    int i = 0;
    while (payload[i] > 13) {
       resep += (char)(payload[i++]);
    }
    if (!resep.equals("error")) {
      Serial.println(resep);
      String dispensed = "";
      // tunggu sampai resep selesai diproses, dan terima input kembali
      unsigned long now = millis();
      while ((millis() - now) <= 300000) { // 5 menit
        dispensed = Serial.readString();
        if (!dispensed.equals("")) {
          break;
        }
      }

      // publish status dispensed nya ke topicFromMachine2
      if(!client.connected()){
        initPubSubClient();
      }
      client.publish(topicFromMachine2, (token.substring(0,6) + "," + dispensed).c_str());

      // end
      resetPublished1();
    } else {
      resetPublished1();
    }
}

void initPubSubClient() {
  client.setKeepAlive(3600);
  client.setServer(mqtt_broker, mqtt_port);
  client.setCallback(callback);
  while (!client.connect("arduinoClient", mqtt_username, mqtt_password)) {}

  client.subscribe(topicToMachine1);
}

void setup() {
  initWifi();
  initPubSubClient();
  Serial.begin(9600);
}

void loop() {
  while (token.length() < 6) {
    token = Serial.readString();
  }
  if (!published1) {
    published1 = 1;
    if(!client.connected()){
      initPubSubClient();
    }
    client.publish(topicFromMachine1, token.c_str());
    lastPublished1 = millis();
  } else {
    if ((millis() - lastPublished1) >= 5000) {
      resetPublished1();
    }
  }
  client.loop();
}
