#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>
#include <Servo.h> 

int servoAtasPin = 22;
int servoBawahPin = 23;

bool dispensedFlag = 0;

int laserReceiverPin = 3;

int irSensorPin = 50;

int helloState = 1;

Servo servoAtas, servoBawah; 

LiquidCrystal_I2C lcd(0x27,20,4);

String token = "";

unsigned long lastInput = 0;

void initLCD() {
  lcd.init();
  lcd.init();
  lcd.backlight();
  lcd.setCursor(4,0);
  lcd.print("Proyek Akhir");
  lcd.setCursor(2,1);
  lcd.print("Sistem  Tertanam");
  lcd.setCursor(2,2);
  lcd.print("Ganjil 2021/2022");
  lcd.setCursor(2,3);
  lcd.print("Samuel -- Prajna");
  delay(2500);
  lcd.clear();
}

void hello() {
  lcd.setCursor(0,0);
  lcd.print("Hello :)      ");
  lcd.setCursor(0,1);
  lcd.print("Press * to continue");
}

const byte ROWS = 4;
const byte COLS = 3;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {30, 31, 32, 33};
byte colPins[COLS] = {40, 41, 42};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

void beginTransaction() {
  helloState = 0;
  lcd.clear();
  lastInput = millis();
}

void initServo() {
  servoAtas.attach(servoAtasPin);
  servoBawah.attach(servoBawahPin);
  servoAtas.write(0);
  servoBawah.write(0);
}

void changeDispenseStatus() {
  dispensedFlag = true;
}

void initInterrupt() {
  pinMode(laserReceiverPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(laserReceiverPin), changeDispenseStatus, FALLING);
}

void initIR() {
  pinMode(irSensorPin, INPUT);
}

void toHelloState() {
  // reset to hello state
  helloState = 1;
  token = "";
}

void setup() {
  initLCD();
  initServo();
  initIR();
  initInterrupt();
  Serial.begin(9600);

  toHelloState();
}

int posisiservoAtas[4] = {32, 64, 96, 124};
int posisiservoBawah[4] = {125, 96, 63, 34};

void dispense(int jenis) {
  servoBawah.write(150);
  delay(1500);
  servoAtas.write(posisiservoAtas[jenis]);
  delay(1500);
  servoBawah.write(posisiservoBawah[jenis]);
  delay(1500);
  servoBawah.write(150);
  delay(1500);
  servoAtas.write(0);
  delay(1500);
  servoBawah.write(0);
  delay(1500);
};

int dispenseQty(int jenis, int qty) {
  while(digitalRead(irSensorPin) == 1) {}
  int dispensed = 0;
  int failureCount = 0;
  while(dispensed < qty) {
    dispense(jenis);
    if (dispensedFlag == true) {
      dispensed++;
      failureCount = 0;
      dispensedFlag = false;
    } else {
      failureCount++;
    }
    if (failureCount == 2) {
      break;
    }
  }
  return dispensed;
}

int strToJenis(String s) {
  // hardcode obat
  // consider do some mapping in the future
  if (s.equals("A")) {
    return 0;
  }
  if (s.equals("B")) {
    return 1;
  }
  if (s.equals("C")) {
    return 2;
  }
  return 3;
}

int strToInt(String s) {
  int res = 0;
  for (int i = 0; i < s.length(); i++) {
    res *= 10;
    res += ((int)s.charAt(i)) - 48;
  }
  return res;
}

String intToString(int x) {
  String res = "";
  while (x > 0) {
    res = ((char)((x % 10) + 48)) + res;
    x /= 10;
  }
  return res;
}

void getResep() {
  Serial.println(token.c_str());
  unsigned long now = millis();
  String resep = "";
  while ((millis() - now) <= 5000) {
    resep = Serial.readString();
    if (!resep.equals("")) {
      break;
    }
  }
  if (resep.equals("")) {
    toHelloState();
    lcd.clear();
    lcd.setCursor(2,0);
    lcd.print("An error occured.");
    lcd.setCursor(2,1);
    lcd.print("Please try again.");
    delay(3000);
    lcd.clear();
  } else {
    // TODO: do dispense based on parsed resep
    int jenis = 0;
    int jumlah = 0;
    String tmp = "";
    
    String res = "";

    for (int i = 0; i < resep.length(); i++) {
      char charNow = resep.charAt(i);
      if (charNow == ':') {
        jenis = strToJenis(tmp);
        res += tmp;
        res += ":";
        tmp = "";
      } else if (charNow == ',') {
        jumlah = strToInt(tmp);
        tmp = "";

        int dispensed = dispenseQty(jenis, jumlah);
        res += intToString(dispensed);
        res += ",";
      } else if (((int)charNow) > 13){
        tmp += charNow;
      }
    }

    // last
    jumlah = strToInt(tmp);
    tmp = "";
    int dispensed = dispenseQty(jenis, jumlah);
    res += intToString(dispensed);

    Serial.println(res);
    lcd.clear();
    lcd.setCursor(2,0);
    lcd.print("Thank you :)");
    delay(3000);
    toHelloState();
  }
}

void insertToken() {
  if (token.length() == 6) {
    // get the resep if token length is 6
    lcd.setCursor(2,0);
    lcd.print("Insert token:");
    lcd.setCursor(2,1);
    lcd.print(token.c_str());
    delay(1000);
    lcd.setCursor(2,0);
    lcd.print("Please wait...");

    getResep();
  } else {
    // input token until 6 digits
    lcd.setCursor(2,0);
    lcd.print("Insert token:");
    lcd.setCursor(2,1);
    lcd.print(token.c_str());

    char customKey = customKeypad.getKey();
    if (customKey) {
      lastInput = millis();
      token += customKey;
    }
  }
}

void loop() {
  if(helloState) {
    hello();
    char customKey = customKeypad.getKey();
    if (customKey == '*') {
      beginTransaction();
    }
  } else {
    insertToken();
    if ((millis() - lastInput) > 5000) {
      toHelloState();
    }
  }
}
