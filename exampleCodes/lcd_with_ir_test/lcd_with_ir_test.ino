//YWROBOT
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

const int pinIRd = 30;
int IRValD = 0;

void setup()
{
  pinMode(pinIRd, INPUT);
  lcd.init();                      // initialize the lcd 
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(3,0);
  lcd.print("Hello, world!");
  lcd.setCursor(2,1);
  lcd.print("Ywrobot Arduino!");
  lcd.setCursor(0,2);
  lcd.print("Arduino LCM IIC 2004");
  lcd.setCursor(2,3);
  lcd.print("Power By Ec-yuan!");
  delay(3000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("IR Status");
}


void loop()
{
  IRValD = digitalRead(pinIRd);
  lcd.setCursor(0,1);
  lcd.print(IRValD);
  delay(500);
}
