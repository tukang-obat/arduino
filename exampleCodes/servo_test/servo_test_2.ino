#include <Servo.h> 

int servoAtasPin = 22;
int servoBawahPin = 23;

Servo ServoAtas, ServoBawah; 

void setup() {
   ServoAtas.attach(servoAtasPin);
   ServoBawah.attach(servoBawahPin);
   ServoAtas.write(0);
   ServoBawah.write(0);

   Serial.begin(9600);
}

int stringToInt(String s) {
  int ans = 0;
  for (int i = 0; i < s.length(); i++) {
    int tmp = (int)(s.charAt(i));
    if (48 <= tmp && tmp <= 57) {
      ans *= 10;
      ans += tmp - 48; 
    }
  }
  return ans;
}

void loop(){ 
  if(Serial.available() > 0) {
    String incoming = Serial.readString();
    int incomingInt = stringToInt(incoming);
    
    if (incoming.charAt(0) == 'A') {
      Serial.println("Moved ServoAtas to:");
      ServoAtas.write(incomingInt);
    } else if (incoming.charAt(0) == 'B') {
      Serial.println("Moved ServoBawah to:");
      ServoBawah.write(incomingInt);
    }
    Serial.println(incomingInt);
  }
}
