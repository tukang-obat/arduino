#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 3;

const byte interruptPin = 2;
unsigned long lastInsideInterrupt = 0;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {30, 31, 32, interruptPin};
byte colPins[COLS] = {40, 41, 42};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

void pin2Interrupt(void)
{
  unsigned long now = millis();
  if (now - lastInsideInterrupt < 1000) {
    return;
  }
  lastInsideInterrupt = now;
  Serial.println("Yey");
  char newKey;
  while (!newKey) {
    newKey = customKeypad.getKey();
  }
  Serial.println(newKey);
  Serial.println("OUT");
}

void setup() {
  Serial.begin(9600);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), pin2Interrupt, RISING);
}

void loop() {
  char customKey = customKeypad.getKey();

  if (customKey) {    
    Serial.println(customKey);
  }
}
