#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 3;

const byte interruptPin = 2;
unsigned long lastInsideInterrupt = 0;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {30, 31, 32, interruptPin};
byte colPins[COLS] = {40, 41, 42};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

int helloState = 1;

void pin2Interrupt(void)
{
  unsigned long now = millis();
  if ((now - lastInsideInterrupt < 1000) || (helloState == 0)) {
    return;
  }
  lastInsideInterrupt = now;
  helloState = 0;
  Serial.println(helloState);
}

void hello() {
  Serial.println("Hello");
  delay(1000);
}

void setup() {
  Serial.begin(9600);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), pin2Interrupt, RISING);
}

void insertToken() {
  char customKey = customKeypad.getKey();
  if (customKey) {
    Serial.println(customKey);
    lastInsideInterrupt = millis();
  }
  if ((millis() - lastInsideInterrupt) > 5000) {
    helloState = 1;
  }
}

void loop() {

  if (helloState == 1) {
    hello();
  } else {
    insertToken();
  }
}
