void setup() {
  pinMode(30, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(30, HIGH);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
  digitalWrite(30, LOW);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
}
