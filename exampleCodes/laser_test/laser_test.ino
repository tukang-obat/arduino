const int laserPin = 50;
const int receiverPin = 52;

void setup() {
  pinMode(laserPin, OUTPUT);
  pinMode(receiverPin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(laserPin, HIGH);
}

void loop() {
  int value = digitalRead(receiverPin);
  if(value == 1) {
    digitalWrite(LED_BUILTIN, HIGH);
  } else if(value == 0) {
    digitalWrite(LED_BUILTIN, LOW);
  }
  delay(100);
}
