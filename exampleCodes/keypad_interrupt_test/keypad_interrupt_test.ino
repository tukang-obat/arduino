#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 3;

const byte interruptPin = 2;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {30, 31, 32, 2};
byte colPins[COLS] = {40, 41, 42};

void setup() {
  Serial.begin(9600);
}

void isr1() {
  Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);
  char customKey = customKeypad.getKey();
  for(int i = 0; i < 10; i++) {
    Serial.println(i);
  }
  pinMode(42, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  pinMode(42, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), isr1, CHANGE);
}