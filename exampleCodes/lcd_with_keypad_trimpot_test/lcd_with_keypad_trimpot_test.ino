//YWROBOT
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 3;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {30, 31, 32, 33};
byte colPins[COLS] = {40, 41, 42};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

int indexX = 0, indexY = 0;

int potPin = 50;
int potVal = 0;

void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(4,0);
  lcd.print("Proyek Akhir");
  lcd.setCursor(2,1);
  lcd.print("Sistem  Tertanam");
  lcd.setCursor(2,2);
  lcd.print("Ganjil 2021/2022");
  lcd.setCursor(2,3);
  lcd.print("Samuel -- Prajna");
  delay(3000);
  lcd.clear();
  lcd.setCursor(0,2);
  lcd.print("Pot value: ");
  lcd.setCursor(0,0);
}


void loop()
{
  char customKey = customKeypad.getKey();

  if (customKey) {
    lcd.setCursor(indexX, indexY);
    lcd.print(customKey);
    indexX++;
    indexX %= 20;
    if (indexX == 0) {
      indexY++;
      indexY %= 2;
    }
  }

  potVal = analogRead(potPin);
  lcd.setCursor(11,2);
  lcd.print(potVal);
}
